# 系统主题SIG组

系统主题SIG组（UKUITheme SIG）致力于帮助社区用户进行与系统主题相关的软件包的参与使用，且帮助用户实现个性化自主打包。

## 工作目标

- 通过本SIG组了解系统主题相关知识
- 通过本SIG组进行个性系统主题打包

## 包列表

- openkylin-theme
- openkylin-wallpapers
- dmz-cursor-theme
- ubuntukylin-theme
- ubuntukylin-wallpapers
- ukui-theme-customizer
- taokylin-theme
- origin-theme
- time-theme


## SIG 成员

### Owner
- kwwzzz (duankaiwen@kylinos.cn)

### Maintainer
- likehomedream

## 邮件列表

- ukuitheme@lists.openkylin.top
