
## SIG说明

开放麒麟社区，青霜框架技术兴趣小组，包括Web引擎、Web工具、浏览器等


## SIG目标
经孵化成熟后，作为麒麟系统的基础组件，集成进发行版中。

# 项目一：

## 项目名称
- 青霜引擎，新形态、轻量化、微内核Web引擎，直接对标CEF，间接对标Electron


## 项目介绍
- 重度孵化 —— 中科院软件所及工信部信发司重点孵化项目，是其标杆创业项目
- 技术验证 —— 客户端运行总数超一亿台；
- 技术形态 —— 1) 国内最极致的微内核MicroKernel架构；2) 插件化；3) 智能伸缩Web引擎；4) 分布式Web引擎设计
- 效果指标 —— 1) 体积：1/10；2) 性能：持平；3) Web兼容度：90%；4) 目前为全球最轻量web引擎
- 官网 —— http://www.rooooot.com/


## 代码仓库
- https://gitee.com/beijing-root/qingshuang


## SIG管理员
- [@ampereufo](https://gitee.com/ampereufo)


## SIG成员
- [@nickehu](https://gitee.com/nickehu)
- [@Cai_Huan](https://gitee.com/Cai_Huan)
- [@yuan-jie](https://gitee.com/yuan-jie)


## SIG邮件列表
- 待定

# 项目二：

# 项目三：