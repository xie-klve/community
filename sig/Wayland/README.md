# Wayland(SIG)
openKylin Wayland小组致力于新一代图形显示服务器相关技术研究，包括wayland合成器、X兼容等。提供wayland相关软件包的技术规划、设计、开发、维护和升级服务，共同推动新一代图形服务器技术发展及落地。 

## 工作目标
1. 自研openKylin wayland 合成器：实现基于wlroots kylin-wayland-compositor(简称wlcom) 
2. 负责openKylin wayland 图栈相关技术发展和决策 
3. 负责openKylin wayland 图栈相关软件包的规划、升级和维护 
4. 及时响应openKylin wayland 图栈相关用户反馈及问题解决

## SIG成员
### Owner
+ miczy（wangyongjun）
### Maintainers
+ ylljhpqj（zhoulei）
+ kylin0061（chenlinxiang）
+ yiqiang（yiqiang）
+ sunzhuy（sunzhigang）
+ apenper（liuyihu）

### Contributors
+ sunlight-open
+ sosaxu
+ xia-123361
+ yangcanfen
+ situation
+ lihongtao123456
+ binzemin
+ rick511


### SIG维护包列表
+ wayland-protocols
+ wayland
+ libseat
+ wlroots
+ xwayland
+ kylin-wayland-compositor

### 邮件列表
wayland@lists.openkylin.top