# 互联互通 SIG

致力于openKylin社区的互联互通基础能力开发与维护

# SIG 职责和目标
- 系统互联互通能力规划及开发
- 系统互联互通兼容性、前瞻性探索及研究

## SIG 成员
### Owner
- zhaoyang1@kylinos.cn

### Maintainers
- huheng@kylinos.cn
- songqianpeng@kylinos.cn
- tangrui@kylinos.cn
- wangrong1@kylinos.cn
- zhaoyang1@kylinos.cn

## 邮件列表
connectivity@lists.openkylin.top
