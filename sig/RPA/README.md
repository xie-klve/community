```text
title: RPA
description: Robotic process automation
published: true
date: 2022-08-31 19:00:11
tags: 
editor: lyl@risencn.com
dateCreated: 2022-08-31 19:00:27
```

## Robotic process automation

机器人流程自动化（Robotic process automation，简称RPA）是以软件机器人及人工智能（AI）为基础的业务过程自动化科技。
机器人流程自动化（RPA）系统是一种应用程序，它通过模仿最终用户在电脑的手动操作方式，提供了另一种方式来使最终用户手动操作流程自动化。

## 工作目标
+ RPA需求收集
+ RPA详细设计
+ RPA研发

## SIG成员

#### owner:　
- leibing(leibing@risencn.com) 
- lyl(lyl@risencn.com) 
- xjj(xjj@risencn.com)

#### maintainers:　
- leibing
- lyl
- xjj

## 邮件列表
