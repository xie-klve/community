```text
title: Notify
description: 消息通知助手
published: true
date: 2022-08-31 19:27:21
tags: 
editor: markdown
dateCreated: 2022-08-31 19:27:25
```

## Notify

实现统一的终端消息提醒服务。

## 工作目标

+ Notify需求收集
+ Notify详细设计
+ Notify研发

## SIG成员

#### owner:　
- leibing(leibing@risencn.com) 
- lyl(lyl@risencn.com) 
- xjj(xjj@risencn.com)

#### maintainers:　
- leibing
- lyl
- xjj

## 邮件列表
