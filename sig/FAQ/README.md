# FAQ 特别兴趣小组

## 简介

FAQ SIG 负责帮助社区用户问题解决，搭建社区知识库


## 工作目标

收集各渠道开发者、爱好者等用户反馈的问题，并建立相关标准化流程推动问题解答或解决同时，在这一过程中不断为 openKylin 社区积累FAQ知识库

1、标准化问题收集、推动解决、结果反馈等整个流程

2、搭建社区知识库，形成标准化问题答疑或操作手册

3、宣传社区已搭建的平台，培养用户主动使用各种平台的习惯


## SIG规范&文档

[SIG规范](https://gitee.com/openkylin/faq-specification)
[SIG文档](https://gitee.com/openkylin/faq-docs)


## SIG成员

### Owner

- [DSOE1024](https://gitee.com/DSOE1024)
- [zhangtianxiong](https://gitee.com/kiber)

### Maintainers

- [zhangtianxiong](https://gitee.com/kiber)
- [kangyanhong](https://gitee.com/kang_yan_hong)
- [江同学](https://gitee.com/JiangZLY)


## 工作流程

![输入图片说明](工作流程.png)